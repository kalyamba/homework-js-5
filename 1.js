function createNewUser() {
  let firstName = prompt("Введите имя:");
  let lastName = prompt("Введите фамилию:");

  let newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      return `${this.firstName.toLowerCase()} ${this.lastName.toLowerCase()}`
    }
  };

  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());